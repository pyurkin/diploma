# Service mesh

Date: 2024-09-10

## Status

Proposed

## Context

Требуется обеспечить безопасность и надежность микросервисов, а также упростить их мониторинг и управление (при переезде мониторинга перестал видеться сервис)

## Decision

В качестве сервис-меша будет использоваться Consul

## Consequences

Потребуется настроить отказоустойчивый кластер Consul и интегрировать в него мониторинг

