# Logging system

Date: 2023-04-18

## Status

Accepted

## Context

Требуется мониторить логи из расчета 1 сообщение (1 Кб) в секунду сейчас и 1500 сообщений (2 Кб) в секунду через три месяца

## Decision

В соответствии с [анализом цен](https://docs.google.com/spreadsheets/d/1uaSHC8-Qz9sWdHDfj6uneO56Y53rUXhB5F0KTZN3_sY/edit) принято решение использовать Sematext 

## Consequences

Анализ цен стоит пересмотреть в дальнейшем
