provider "yandex" {
  cloud_id  = "b1geqv1mf9rtifcrlvfc"
  folder_id = "b1grmabjeidsgp1u60ar"
  zone      = "ru-central1-a"
}

module "server" {
  source                           = "../modules/vm"
  gitlab_runner_registration_token = var.gitlab_runner_registration_token
  ansible_deploy_path              = "../ansible/deploy"
  ansible_monitoring_path          = "../ansible/monitoring"
  app_host                         = "stage-app.deve.host"
  app_monitoring_host              = "stage-app-monitoring.deve.host"
  app_grafana_host                 = "stage-app-grafana.deve.host"
  resource_prefix                  = "stage-app"
}
