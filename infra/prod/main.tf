provider "yandex" {
  service_account_key_file = "key.json"
  cloud_id                 = "b1geqv1mf9rtifcrlvfc"
  folder_id                = "b1grmabjeidsgp1u60ar"
  zone                     = "ru-central1-a"
}

resource "yandex_vpc_network" "app-network" {
  name = "app-network"
}

resource "yandex_vpc_subnet" "app-subnet" {
  name           = "app-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.app-network.id
  v4_cidr_blocks = ["10.0.0.0/24"]
}

module "application" {
  source    = "../modules/vm"
  subnet_id = yandex_vpc_subnet.app-subnet.id
  #   gitlab_runner_registration_token = var.gitlab_runner_registration_token
  ansible_deploy_path     = "../ansible/deploy"
  ansible_monitoring_path = "../ansible/monitoring"
  app_host                = "app.deve.host"
  app_monitoring_host     = "app-monitoring.deve.host"
  app_grafana_host        = "app-grafana.deve.host"
  resource_prefix         = "application"
}

module "consul-first" {
  source             = "../modules/consul"
  name_suffix        = "01"
  subnet_id          = yandex_vpc_subnet.app-subnet.id
  ansible_public_key = module.application.ssh_key_public
}

module "consul-second" {
  source             = "../modules/consul"
  name_suffix        = "02"
  subnet_id          = yandex_vpc_subnet.app-subnet.id
  ansible_public_key = module.application.ssh_key_public
}

module "consul-third" {
  source             = "../modules/consul"
  name_suffix        = "03"
  subnet_id          = yandex_vpc_subnet.app-subnet.id
  ansible_public_key = module.application.ssh_key_public
}

resource "local_file" "ssh-key-file" {
  filename        = "../ansible/id_ed25519"
  content         = module.application.ssh_key_private
  file_permission = "0600"
}

resource "local_file" "consul-inventory" {
  content = templatefile("inventory.tftpl", {
    section_name = "consul_instances",
    instances = [
      {
        ip     = module.consul-first.public_ip
        config = "consul_node_role=bootstrap consul_bind_address=${module.consul-first.internal_ip} use_proxy=false"
      },
      {
        ip     = module.consul-second.public_ip
        config = "consul_node_role=server consul_bind_address=${module.consul-second.internal_ip} use_proxy=false"
      },
      {
        ip     = module.consul-third.public_ip
        config = "consul_node_role=server consul_bind_address=${module.consul-third.internal_ip} use_proxy=false"
      },
      {
        ip     = module.application.public_ip
        config = "consul_node_role=client consul_bind_address=${module.application.internal_ip} use_proxy=false"
      },
      {
        ip     = module.application.monitoring-public-ip
        config = "consul_node_role=client consul_bind_address=${module.application.monitoring-internal-ip} use_proxy=false"
      },
    ]
  })
  filename = "../ansible/inventory/consul"
}

resource "local_file" "monitoring-inventory" {
  content = templatefile("inventory.tftpl", {
    section_name = "monitoring_instances",
    instances = [
      {
        ip     = module.application.monitoring-public-ip
        config = ""
      }
    ]
  })
  filename = "../ansible/inventory/monitoring"
}

resource "local_file" "consul-template-inventory" {
  content = templatefile("inventory.tftpl", {
    section_name = "consul_template_instances",
    instances = [
      {
        ip     = module.application.monitoring-public-ip
        config = ""
      }
    ]
  })
  filename = "../ansible/inventory/consul_template"
}

resource "local_file" "app-inventory" {
  content = templatefile("inventory.tftpl", {
    section_name = "app_instances",
    instances = [
      {
        ip     = module.application.public_ip
        config = ""
      }
    ]
  })
  filename = "../ansible/inventory/app"
}
