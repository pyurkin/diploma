FROM ubuntu:latest

RUN apt-get update && apt-get install -y openssh-server \
    && mkdir /var/run/sshd

RUN apt-get update && apt-get install -y \
    openssh-server \
    python3 \
    python3-pip \
    sudo

RUN echo "root ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN apt-get install -y python3-apt
RUN apt-get install -y net-tools
RUN apt-get install -y iproute2

RUN sed -i 's/#PermitEmptyPasswords no/PermitEmptyPasswords yes/' /etc/ssh/sshd_config
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN passwd -d root

EXPOSE 22
EXPOSE 8500
EXPOSE 8600

CMD ["/usr/sbin/sshd", "-D"]

# docker build -t srv -f Srv.Dockerfile .
# docker network create --driver bridge srv-net
# docker stop srv-1 && docker rm srv-1
# docker run -dit --name srv-1 --network srv-net --publish 8500:8500 --publish 8600:8600 srv
# docker exec -it srv-1 bash
