resource "tls_private_key" "ssh-key" {
  algorithm = "ED25519"
}

resource "local_file" "ssh-key-file" {
  filename        = "id_ed25519"
  //noinspection HILUnresolvedReference
  content         = tls_private_key.ssh-key.private_key_openssh
  file_permission = "0600"
}
