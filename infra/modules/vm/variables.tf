variable "resource_prefix" {
  type        = string
  default     = "app"
  description = "Resources prefix"
}

variable "subnet_id" {
  type        = string
  description = "Subnet ID"
}

variable "app_host" {
  type        = string
  default     = "test.deve.host"
  description = "Application host"
}

variable "app_monitoring_host" {
  type        = string
  default     = "app-monitoring.deve.host"
  description = "Application monitoring host"
}

variable "app_grafana_host" {
  type        = string
  default     = "app-grafana.deve.host"
  description = "Application grafana host"
}

# variable "gitlab_runner_registration_token" {
#   type        = string
#   sensitive   = true
#   nullable    = false
#   description = "Runner registration token from https://gitlab.com/<username>/<project>/-/settings/ci_cd"
#   validation {
#     condition     = length(var.gitlab_runner_registration_token) == 29
#     error_message = "Runner registration token not found"
#   }
# }

variable "ansible_deploy_path" {
  type        = string
  nullable    = false
  description = "Relative local path to ansible/deploy directory"
}

variable "ansible_monitoring_path" {
  type        = string
  nullable    = false
  description = "Relative local path to ansible/monitoring directory"
}
