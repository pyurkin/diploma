resource "yandex_compute_instance" "app-vm" {
  name        = "${var.resource_prefix}-vm"
  platform_id = "standard-v3"

  resources {
    core_fraction = 20
    cores         = 2
    memory        = 1
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      type     = "network-hdd"
      size     = 10
      image_id = "fd8cg4hn26sqsbj8p4mj" # Ubuntu 24.04 LTS
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    user-data = templatefile("cloud-config.yml", {
      ansible_authorized_key = tls_private_key.ssh-key.public_key_openssh
    })
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -yq",
      "sudo mkdir -p /var/www/",
      "sudo chown -R :www-data /var/www",
      "sudo chmod 775 /var/www",
    ]

    connection {
      type        = "ssh"
      user        = "ansible"
      host        = self.network_interface.0.nat_ip_address
      //noinspection HILUnresolvedReference
      private_key = tls_private_key.ssh-key.private_key_openssh
    }
  }

#   provisioner "local-exec" {
#     command = <<-EOT
#       ansible-galaxy install -r ${var.ansible_deploy_path}/requirements.yml
#       ansible-playbook -u ansible -i '${self.network_interface.0.nat_ip_address},' \
#         --private-key=${local_file.ssh-key-file.filename} \
#         --extra-vars='gitlab_runner_registration_token=${var.gitlab_runner_registration_token}' \
#         --extra-vars='runner_tag=${var.resource_prefix}' \
#         ${var.ansible_deploy_path}/playbook.yml
#     EOT
#     # env ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -u ansible -i '130.193.51.95,' --private-key=id_ed25519 playbook.yml
#     environment = {
#       ANSIBLE_HOST_KEY_CHECKING = "false"
#     }
#   }
}

output "public_ip" {
  value = yandex_compute_instance.app-vm.network_interface.0.nat_ip_address
}

output "internal_ip" {
  value = yandex_compute_instance.app-vm.network_interface.0.ip_address
}

output "ssh_key_private" {
  value = tls_private_key.ssh-key.private_key_openssh
}

output "ssh_key_public" {
  value = tls_private_key.ssh-key.public_key_openssh
}
