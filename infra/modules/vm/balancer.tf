resource "yandex_lb_target_group" "app-target-group" {
  name      = "${var.resource_prefix}-target-group"
  region_id = "ru-central1"

  target {
    subnet_id = var.subnet_id
    address   = yandex_compute_instance.app-vm.network_interface.0.ip_address
  }
}

resource "yandex_lb_network_load_balancer" "app-load-balancer" {
  name = "${var.resource_prefix}-load-balancer"

  listener {
    name        = "${var.resource_prefix}-load-balancer-listener"
    port        = 80
    target_port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.app-target-group.id

    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/"
      }
    }
  }
}
