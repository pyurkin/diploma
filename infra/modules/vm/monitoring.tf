resource "yandex_compute_instance" "monitor-vm" {
  name        = "${var.resource_prefix}-monitor-vm"
  platform_id = "standard-v3"

  resources {
    core_fraction = 20
    cores         = 2
    memory        = 1
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      type     = "network-hdd"
      size     = 10
      image_id = "fd8cg4hn26sqsbj8p4mj" # Ubuntu 24.04 LTS
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    user-data = templatefile("cloud-config.yml", {
      ansible_authorized_key = tls_private_key.ssh-key.public_key_openssh
    })
  }

  provisioner "remote-exec" {
    inline = ["sudo apt-get update -yq"]

    connection {
      type        = "ssh"
      user        = "ansible"
      host        = self.network_interface.0.nat_ip_address
      //noinspection HILUnresolvedReference
      private_key = tls_private_key.ssh-key.private_key_openssh
    }
  }

#   provisioner "local-exec" {
#     command = <<-EOT
#       ansible-playbook -u ansible -i '${self.network_interface.0.nat_ip_address},' \
#         --private-key=${local_file.ssh-key-file.filename} \
#         -e='target=${var.app_host}' \
#         -e='app_monitoring_host=${var.app_monitoring_host}' \
#         -e='app_grafana_host=${var.app_grafana_host}' \
#         ${var.ansible_monitoring_path}/playbook.yml
#     EOT
#     environment = {
#       ANSIBLE_HOST_KEY_CHECKING = "false"
#     }
#   }
}

output "monitoring-public-ip" {
  value = yandex_compute_instance.monitor-vm.network_interface.0.nat_ip_address
}

output "monitoring-internal-ip" {
  value = yandex_compute_instance.monitor-vm.network_interface.0.ip_address
}
