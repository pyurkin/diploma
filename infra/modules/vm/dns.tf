data "yandex_dns_zone" "deve-host" {
  name = "deve-host-public"
}

resource "yandex_dns_recordset" "test-rs" {
  zone_id = data.yandex_dns_zone.deve-host.id
  name    = "${var.app_host}."
  type    = "A"
  ttl     = 600
  data    = yandex_lb_network_load_balancer.app-load-balancer.listener.*.external_address_spec[0].*.address
}

resource "yandex_dns_recordset" "monitoring-rs" {
  zone_id = data.yandex_dns_zone.deve-host.id
  name    = "${var.app_monitoring_host}."
  type    = "A"
  ttl     = 600
  data    = [yandex_compute_instance.monitor-vm.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "grafana-rs" {
  zone_id = data.yandex_dns_zone.deve-host.id
  name    = "${var.app_grafana_host}."
  type    = "A"
  ttl     = 600
  data    = [yandex_compute_instance.monitor-vm.network_interface.0.nat_ip_address]
}
