# resource "yandex_vpc_network" "consul-network" {
#   name = "consul-network"
# }
#
# resource "yandex_vpc_subnet" "consul-subnet" {
#   name           = "consul-subnet"
#   zone           = "ru-central1-a"
#   network_id     = yandex_vpc_network.consul-network.id
#   v4_cidr_blocks = ["10.0.0.0/24"]
# }

# resource "yandex_vpc_security_group" "consul_security_group" {
#   name        = "consul-security-group"
#   network_id  = yandex_vpc_network.consul-network.id
#
#   # Правило для серверного трафика Consul (TCP 8300)
#   ingress {
#     protocol       = "TCP"
#     description    = "Allow Consul server traffic on port 8300"
#     v4_cidr_blocks = ["10.0.0.0/24"]
#     port           = 8300
#   }
#
#   # Правило для LAN-соединений между узлами Consul (TCP и UDP 8301)
#   ingress {
#     protocol       = "TCP"
#     description    = "Allow LAN traffic on port 8301"
#     v4_cidr_blocks = ["10.0.0.0/24"]
#     port           = 8301
#   }
#   ingress {
#     protocol       = "UDP"
#     description    = "Allow LAN traffic on port 8301 (UDP)"
#     v4_cidr_blocks = ["10.0.0.0/24"]
#     port           = 8301
#   }
#
#   # Правило для WAN-соединений между узлами Consul (TCP и UDP 8302)
#   ingress {
#     protocol       = "TCP"
#     description    = "Allow WAN traffic on port 8302"
#     v4_cidr_blocks = ["10.0.0.0/24"]
#     port           = 8302
#   }
#   ingress {
#     protocol       = "UDP"
#     description    = "Allow WAN traffic on port 8302 (UDP)"
#     v4_cidr_blocks = ["10.0.0.0/24"]
#     port           = 8302
#   }
#
#   # Правило для API и веб-интерфейса Consul (TCP 8500)
#   ingress {
#     protocol       = "TCP"
#     description    = "Allow API and UI traffic on port 8500"
#     v4_cidr_blocks = ["0.0.0.0/0"]
#     port           = 8500
#   }
#
#   # Правило для DNS-запросов Consul (TCP и UDP 8600)
#   ingress {
#     protocol       = "TCP"
#     description    = "Allow DNS traffic on port 8600 (TCP)"
#     v4_cidr_blocks = ["10.0.0.0/24"]
#     port           = 8600
#   }
#   ingress {
#     protocol       = "UDP"
#     description    = "Allow DNS traffic on port 8600 (UDP)"
#     v4_cidr_blocks = ["10.0.0.0/24"]
#     port           = 8600
#   }
# }
