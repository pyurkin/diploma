resource "yandex_compute_instance" "consul-vm" {
  name        = "consul-vm-${var.name_suffix}"
  platform_id = "standard-v3"

  resources {
    core_fraction = 20 # % CPU
    cores         = 2
    memory        = 1
  }

  scheduling_policy {
    preemptible = true # прерываемая
  }

  boot_disk {
    initialize_params {
      type     = "network-hdd"
      size     = 10
      image_id = "fd8cg4hn26sqsbj8p4mj" # Ubuntu 24.04 LTS
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    user-data             = templatefile("cloud-config.yml", {
      ansible_authorized_key = var.ansible_public_key
    })
    serial-port-enable    = "1"
    install-unified-agent = "1"
  }
}

output "public_ip" {
  value = yandex_compute_instance.consul-vm.network_interface.0.nat_ip_address
  description = "Public IP address"
}

output "internal_ip" {
  value = yandex_compute_instance.consul-vm.network_interface.0.ip_address
  description = "Internal IP address"
}
