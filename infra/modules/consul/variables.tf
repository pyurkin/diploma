variable "name_suffix" {
  type        = string
  default     = "00"
  description = "Instance name suffix"
}

variable "subnet_id" {
  type        = string
  description = "Subnet ID"
}

variable "ansible_public_key" {
  type        = string
  description = "Ansible authorized key"
}
