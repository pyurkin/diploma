ANSIBLE_TAGS ?= all

ansible-build: ## Сборка образа ansible с volume для хранения ролей
	docker build -t ansible:latest -f infra/ansible/Dockerfile infra/ansible
	docker volume create --name=ansible-data

ansible-requirements: ## (Пере-)установка ansible-ролей
	docker run --rm -v $(PWD)/infra/ansible:/ansible -v ansible-data:/root/.ansible ansible:latest ansible-galaxy install -r ./requirements.yml # --force

terraform-apply-prod: ## Запуск облака через terraform
	cd infra/prod && terraform init && terraform fmt && terraform validate && terraform apply -auto-approve

terraform-destroy-prod: ## Удаление облака через terraform
	cd infra/prod && terraform destroy -auto-approve

ansible-prod: ## Установка consul через ansible (ANSIBLE_TAGS=monitoring)
	docker run --rm -v $(PWD)/infra/ansible:/ansible -v ansible-data:/root/.ansible ansible:latest ansible-playbook playbook.yml --tags=$(ANSIBLE_TAGS)

.DEFAULT_GOAL := help
help: ## Справка
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
