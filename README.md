# Как развернуть проект в облаке

Разработка проекта ведется в отдельном репозитории (например, https://github.com/bhavenger/skillbox-diploma.git).

## предварительно
1. [настроить yc cli](https://cloud.yandex.ru/docs/cli/quickstart) и авторизоваться в yc

#```bash
#export YC_TOKEN=$(yc iam create-token)
#export YC_CLOUD_ID=$(yc config get cloud-id)
#export YC_FOLDER_ID=$(yc config get folder-id)
#```

## развернуть инфраструктуру для нового проекта 
1. создать новый проект gitlab
2. клонировать данный проект и заменить remote на свой
3. сгенерировать `key.json` для сервисного аккаунта `yc iam key create --service-account-name provisioner --folder-name default -o key.json`
4. закинуть содержимое файла `key.json` в переменную типа file с именем `PROVISIONER_KEY_JSON` (`Expand variable reference` is `off`)
5. токен регистрации runner-а закинуть в переменную `RUNNER_REGISTRATION_TOKEN`
6. внести необходимые изменения в настройки окружений (директория infra)
   1. изменить домены
   2. добавить ключ разработчика (`developer`) в файл `meta.yml` (при необходимости)
7. указать репозиторий проекта с приложением в переменной `app.repository` файла `service/ansible/roles/deploy/vars/main.yml`
8. создать ветку stage для тестирования
9. выполнить commit и push

## workflow
1. перейти в последний успешный pipeline, например https://gitlab.com/pyurkin/diploma/-/pipelines/834886385
2. развернуть инфраструктуру (если еще не) "Infrastructure Prod Apply"
3. выполнить deploy приложения "Service Prod Deploy"
4. после тестирования выполнить "Infrastructure Prod Destroy"

## workflow manual
1. зайти на сервер с приложением
2. склонировать проект: `git clone https://gitlab.com/pyurkin/diploma.git && cd diploma`
3. выполнить ansible-playbook: `sudo ansible-playbook service/ansible/playbook.yml`
4. запустить приложение: `cd /var/www/app && sudo docker compose up`
5. перейти в приложение: `http://89.169.137.91:8080/`

## troubleshooting
```
yc compute instance list
yc compute instance get consul-vm
```
```
sudo vim /run/cloud-init/combined-cloud-config.json
sudo cloud-init single --name write_files
sudo cloud-init clean
sudo cloud-init init
sudo cloud-init modules --mode config
sudo cloud-init modules --mode final
```


1. докер при изменении Dockerfile-а собирается в pipeline, но можно собрать локально (или загрузить собранный)
```
docker build -t yc-provisioner infra
docker run -it -v $(pwd)/infra:/data yc-provisioner bash
# comment backend "http"
cd stage/prod
export YC_SERVICE_ACCOUNT_KEY_FILE=../key.json 
export TF_VAR_gitlab_runner_registration_token='XXX' 
gitlab-terraform plan
gitlab-terraform apply
ssh -o StrictHostKeyChecking=no developer@178.154.203.90
git clone https://gitlab.com/pyurkin/diploma.git
cd diploma
ansible-playbook service/ansible/playbook.yml
gitlab-terraform destroy
env ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -u ansible -i '130.193.51.95,' --private-key=id_ed25519 ansible/playbook.yml
```
